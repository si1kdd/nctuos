#!/usr/bin/env bash

# Debug mode
if [ $1 == 1 ];then
	qemu-system-i386 -m 512 -hda kernel.img -monitor stdio -gdb tcp:127.0.0.1:1234 -S
fi

# Release Mode
if [ $1 == 2 ];then
	qemu-system-i386 -m 1024 -hda kernel.img -monitor stdio
fi
