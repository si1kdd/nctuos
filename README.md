# nctuOS

* A little 32bit OS, base on MIT xv6.
  * NCTU Operation System Design and Implementation Course work.

## Features:
* Multi-process support.

## Build:
* (Suppose build on x64 system)
* make all && ./run.sh 2

## Depend on:
* gcc-mulitlib, perl
   * Support version is 6.3.1,
   * You can modify the kernel/kern.ld, and fit your compiler version

## TODO list:
- SMP (Symmetric Multi-Processor)
- Better shell. (Basic command support on Kernel layer and User layer).
- Memory ALSR.
  - ......
